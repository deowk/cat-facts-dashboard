This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

## Run tests

```bash
yarn test
# or
yarn test:ci
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Future Improvements

- Retain search terms when heading back to the Dashboard from a Details page
- Add loader during route transitions for a more responsive feel
- Upgrade responsive styling
- Account for large volumes of data by allowing pagination controls to be cycled instead of displaying all at once
- Add icons to upvote counts
- When searching attempt to stay on current page, if current page is larger than available in new set go to last page
