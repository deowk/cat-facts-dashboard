import { useState, useEffect } from "react";
import styles from "./avatar.module.scss";
import cx from "classnames";

const _loaded = {};

export default function Avatar({ src }) {
  const [loaded, setLoaded] = useState(_loaded[src]);
  const onLoad = () => {
    _loaded[src] = true;
    setLoaded(true);
  };
  useEffect(() => {
    const img = new Image();
    img.onload = onLoad;
    img.src = src;
  }, []);
  return (
    <div
      className={cx(styles.avatar, loaded ? styles.a_loaded : styles.a_loading)}
    >
      <img
        className={loaded ? styles.loaded : styles.loading}
        src={src}
        alt="user icon"
      />
    </div>
  );
}
