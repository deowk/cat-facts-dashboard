import styles from "./pagination.module.scss";
import cx from "classnames";

function paginationControls(onClick, page, count) {
  const pagesArray = [];
  for (let i = 1; i <= count; i++) {
    pagesArray.push(i);
  }
  return pagesArray.map((p) => (
    <span
      className={cx(styles.selector, { active: p == page })}
      onClick={() => onClick(p)}
      key={p}
    >
      {p}
    </span>
  ));
}

export default function Pagination({ onClick, pageSize, page, data }) {
  const pagesCount =
    data.length / pageSize + (data.length % pageSize > 0 ? 1 : 0);
  return pagesCount > 0 ? (
    <div className={styles.container}>
      {paginationControls(onClick, page, pagesCount)}
    </div>
  ) : null;
}
