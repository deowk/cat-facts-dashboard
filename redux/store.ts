import { createStore, AnyAction } from "redux";
import { MakeStore, createWrapper, Context, HYDRATE } from "next-redux-wrapper";

export interface Name {
  first: string;
  last: string;
}

export interface User {
  name: Name;
  _id: string;
}

export interface Cats {
  text: string;
  type: string;
  upvotes: number;
  user: User;
  userUpvoted: null | number;
  _id: string;
}

export interface State {
  server: {
    all: Cats[];
  };
  client: {
    all: Cats[];
  };
}

// create your reducer
export const reducer = (
  state: State = { server: { all: [] }, client: { all: [] } },
  action: AnyAction
) => {
  switch (action.type) {
    case HYDRATE:
      return {
        ...state,
        server: {
          ...state.server,
          ...action.payload.server,
        },
      };
    case "SERVER_ACTION":
      return {
        ...state,
        server: {
          ...state.server,
          ...action.payload,
        },
      };
    case "CLIENT_ACTION":
      return {
        ...state,
        client: {
          ...state.client,
          ...action.payload,
        },
      };
    default:
      return state;
  }
};

// create a makeStore function
const makeStore: MakeStore<State> = (context: Context) => createStore(reducer);

// export an assembled wrapper
export const wrapper = createWrapper<State>(makeStore, { debug: false });
