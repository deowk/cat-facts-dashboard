import Head from "next/head";
import { useState, useMemo, useEffect } from "react";
import { useSelector } from "react-redux";
import { State } from "../redux/store";
import { filter, paginate, highlightSearch } from "../utils/helpers";
import Pagination from "../components/pagination";
import styles from "./styles.module.scss";
import cx from "classnames";
import Link from "next/link";

const PAGE_SIZE = 10;

function Dashboard() {
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(1);
  const state: State = useSelector<State, State>((state) => state);

  const filteredData = useMemo(() => filter(search, state.server.all), [
    search,
  ]);

  const displayData = useMemo(() => {
    return paginate(PAGE_SIZE, page, filteredData);
  }, [filteredData, page]);

  useEffect(() => {
    setPage(1);
  }, [filteredData]);

  return (
    <div className="container">
      <Head>
        <title>Cat Facts Dashboard</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          href="https://fonts.googleapis.com/css2?family=Inconsolata:wght@300;600;800&display=swap"
          rel="stylesheet"
        ></link>
      </Head>
      <main>
        <input
          data-testid="search"
          placeholder="Search"
          className={styles.search}
          type="text"
          value={search}
          onChange={(e) => setSearch(e.currentTarget.value)}
        />
        <div className={styles.results}>
          <div className={styles.header}>
            <div className={cx(styles.heading, styles.user_id)}>User Id</div>
            <div className={cx(styles.heading, styles.firstname)}>
              First Name
            </div>
            <div className={cx(styles.heading, styles.lastname)}>Last Name</div>
            <div className={cx(styles.heading, styles.cat_text)}>Cat Text</div>
            <div className={cx(styles.heading, styles.upvotes)}>Upvotes</div>
          </div>
          <div className={styles.rows}>
            {displayData.length > 0 ? (
              displayData.map((cat) => (
                <div className={styles.row} key={cat._id}>
                  <div className={cx(styles.cell, styles.user_id)}>
                    <Link href={`/cat/${encodeURIComponent(cat._id)}`}>
                      <a className={styles.catLink}>{cat.user?._id}</a>
                    </Link>
                  </div>
                  <div
                    dangerouslySetInnerHTML={highlightSearch(
                      search,
                      cat.user?.name.first
                    )}
                    className={cx(styles.cell, styles.firstname)}
                  />
                  <div
                    dangerouslySetInnerHTML={highlightSearch(
                      search,
                      cat.user?.name.last
                    )}
                    className={cx(styles.cell, styles.lastname)}
                  />
                  <div
                    dangerouslySetInnerHTML={highlightSearch(search, cat.text)}
                    className={cx(styles.cell, styles.cat_text)}
                  />
                  <div className={cx(styles.cell, styles.upvotes)}>
                    {cat.upvotes}
                  </div>
                </div>
              ))
            ) : (
              <p className={styles.noData}>No Data Available</p>
            )}
          </div>
        </div>
        {
          <Pagination
            onClick={setPage}
            page={page}
            data={filteredData}
            pageSize={PAGE_SIZE}
          />
        }
      </main>
      <footer></footer>
    </div>
  );
}

export default Dashboard;
