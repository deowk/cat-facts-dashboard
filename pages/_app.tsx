import React from "react";
import { wrapper } from "../redux/store";
import fetch from "isomorphic-unfetch";
import App, { AppInitialProps, AppContext } from "next/app";
import "../styles/reset.scss";
import "../styles/global.scss";

class CustomApp extends App<AppInitialProps> {
  public static getInitialProps = async ({ Component, ctx }: AppContext) => {
    const res = await fetch(`https://cat-fact.herokuapp.com/facts`);
    const data = await res.json();
    ctx.store.dispatch({ type: "SERVER_ACTION", payload: data });

    return {
      pageProps: {
        // Call page-level getInitialProps
        ...(Component.getInitialProps
          ? await Component.getInitialProps(ctx)
          : {}),
        pathname: ctx.pathname,
      },
    };
  };

  public render() {
    const { Component, pageProps } = this.props;
    return <Component {...pageProps} />;
  }
}

export default wrapper.withRedux(CustomApp);
