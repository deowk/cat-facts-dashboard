import Head from "next/head";
import styles from "./cats.module.scss";
import { useRouter } from "next/router";
import { wrapper, Cats } from "../../redux/store";
import Avatar from "../../components/avatar";

export default function Cat({ details = null }: { details: Cats | null }) {
  const router = useRouter();

  return (
    <div className="container">
      <Head>
        <title>Cat Details</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          href="https://fonts.googleapis.com/css2?family=Inconsolata:wght@300;600;800&display=swap"
          rel="stylesheet"
        />
      </Head>
      <div className={styles.details}>
        <div className={styles.button} onClick={() => router.push("/")}>
          Back
        </div>
        {details === null ? (
          <div className={styles.notFound}>Cat Not Found</div>
        ) : (
          <div className={styles.body}>
            <div className={styles.avatar_container}>
              <Avatar
                src={`https://eu.ui-avatars.com/api/?name=${details.user?.name.first}+${details.user?.name.last}&size=250&background=e9bf75&color=231927`}
              />
            </div>
            <div className={styles.details_container}>
              <h4>{`Added By ${details.user?.name.first} ${details.user?.name.last}`}</h4>
              <p>{details.text}</p>
              <div className={styles.votes}>
                <span>{`Upvotes: ${details.upvotes}`}</span>
                <span>{`User Upvotes: ${
                  details.userUpvoted === null ? 0 : details.userUpvoted
                }`}</span>
              </div>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export const getServerSideProps = wrapper.getServerSideProps(
  async ({ store, query }) => {
    const details = store.getState().server.all.find((cat) => {
      return cat._id === query.pid;
    });
    return {
      props: { details: typeof details === "undefined" ? null : details }, // will be passed to the page component as props
    };
  }
);
