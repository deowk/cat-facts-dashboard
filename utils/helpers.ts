import { Cats } from "../redux/store";

export function filter(search: null | string = "", data: Cats[] = []) {
  const matches: Cats[] = [];
  let testingFunc: () => boolean;
  for (const cat of data) {
    const value = `${cat.text} ${cat.user?.name.first} ${cat.user?.name.last}`;
    try {
      const rxp = new RegExp(search, "gi");
      testingFunc = () => rxp.test(value);
    } catch (e) {
      // In case someone tries to input regex into the search
      testingFunc = () => value.indexOf(search) > 0;
    }
    if (testingFunc()) {
      matches.push(cat);
    }
  }

  return matches;
}

export function paginate(size: number, page: number, data: Cats[]) {
  return data.slice((page - 1) * size, page * size);
}

export function highlightSearch(search: string, val: string = "") {
  try {
    return {
      __html: val.replace(
        new RegExp(search, "gi"),
        (matchedString) =>
          `<strong style="color: #ca3b81">${matchedString}</strong>`
      ),
    };
  } catch (e) {
    return { __html: val };
  }
}
