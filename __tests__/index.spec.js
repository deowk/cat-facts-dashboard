import Dashboard from "../pages/index";
import React from "react";
import { render, fireEvent, screen } from "../utils/test-utils";

const initialState = {
  server: {
    all: [
      {
        _id: "599f87db9a11040c4a16343f",
        text:
          "The goddess of love, beauty, and fertility in Norse mythology, Freyja was the first cat lady. She is depicted in stories as riding a chariot that was drawn by cats.",
        type: "cat",
        user: {
          _id: "5a9ac18c7478810ea6c06381",
          name: { first: "Alex", last: "Wohlbruck" },
        },
        upvotes: 8,
        userUpvoted: null,
      },
    ],
  },
  client: { all: [] },
};

test("renders not available when no match", async () => {
  render(<Dashboard />, { initialState });

  // enter search term
  fireEvent.change(screen.getByTestId(/search/i), {
    target: { value: "ffffffffffffff" },
  });

  expect(screen.getByText("No Data Available")).toBeInTheDocument();
});

test("renders result when match is found", () => {
  render(<Dashboard />, { initialState });

  // enter search term
  fireEvent.change(screen.getByTestId(/search/i), {
    target: { value: "Alex" },
  });

  expect(screen.getByText("5a9ac18c7478810ea6c06381")).toBeInTheDocument();
});
